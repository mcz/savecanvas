from flask import Flask, render_template, request, send_file
import base64
import datetime
from shutil import make_archive

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/success", methods=['POST'])
def success():
    if request.method == 'POST':
        image_str = request.values['imageBase64']
        print(type(image_str))
        image_bytes = base64.b64decode(image_str.encode(encoding='UTF-8', errors='strict'))
        image_name = str(datetime.datetime.today()).replace('.', '_').replace(':', '')
        with open('render/' + image_name + '.png', 'wb') as f:
            f.write(image_bytes)
        return render_template("index.html", text="POSTED")
    return render_template("index.html", text="NOT WORKING")


@app.route("/getfiles", methods=['GET'])
def getfiles():
    return render_template("getfiles.html")


@app.route('/download')
def download():
    root_dir = 'render'
    make_archive('renderzip', 'zip', root_dir)
    return send_file("renderzip.zip",
                     attachment_filename="allrenders.zip",
                     as_attachment=True)


if __name__ == "__main__":
    app.debug = True
    app.run()
